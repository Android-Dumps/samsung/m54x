#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_m54x.mk

COMMON_LUNCH_CHOICES := \
    omni_m54x-user \
    omni_m54x-userdebug \
    omni_m54x-eng
